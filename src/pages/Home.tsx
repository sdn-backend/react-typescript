import React from 'react';
import { useSelector } from 'react-redux';
import { Navigate } from 'react-router-dom';
import { RootState } from '../types';

export default function Home(){
  const token = useSelector<RootState, string | null>((state) => state.auth.token);

  if(token === null) {
    return <Navigate replace to="/signin" />;
  }

  return (<div>
    <h1>Home</h1>
  </div>);
}