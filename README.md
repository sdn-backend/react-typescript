# React + Typescript

## 사용 기술
- react
- typescript
- react-router-dom
- redux
- redux-actions
- redux-saga
- connected-react-router
- ant-design
- axios

## 목차
1. [구현 방법](#1_구현_방법)

## 1 구현 방법
### 1-1. 라우팅 설정
- `react-router-dom` 
- `@types/react-router-dom` 
  - 타입스크립트 사용 시 설치
```shell
npm install --save react-router-dom
npm install --save-dev @types/react-router-dom
```
- 필요한 페이지 라우터 페이지 개발
- `react-error-boundary` 라이브러리 통해 에러 발생 시 페이지 이동

### 1-2. 로그인, 로그아웃
- 로그인 api 비동기 로직 >> `redux` 설정
- 필요 라이브러리 설치
```shell
npm i redux react-redux redux-saga redux-devtools-extension redux-actions
```
- 타입스크립트 사용
```shell
npm i @types/react-redux @types/redux-actions -D
```
- 개발 순서
  1. 스토어 로직 `create.ts`
    - `auth.ts` `reducer.ts`
    - action 필요한 데이터는 `payload` 이름을 사용 !
  2. 페이지 로직 
    - `ant-design` 설치 
    ```shell
    npm i antd
    npm i @ant-design/icons
    ```
    - ant.css 적용
      - `index.tsx` 위에 `import "antd/dist/antd.css"` 추가해준다.
    - api 분리해서 작성
      - `axios` 다운로드 `npm i axios`
    - push 하기 위해 `connected-react-router` 사용
      - `npm i connected-react-router`
    - ...